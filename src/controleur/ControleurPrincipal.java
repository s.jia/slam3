package controleur;
import dao.ConnexionPostgreSql;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import classes.Club;
import classes.Licencie;
import dao.ClubDAO;
import dao.DAO;
import dao.LicencieDAO;
import vues.Vue_principale;

public class ControleurPrincipal implements ActionListener{

	// définition de l’objet instance de MaVue (vue principale)
	private Vue_principale vue;
	// déclaration des objets Modele qui permettront d’obtenir ou de transmettre les données :
	private DAO<Club> gestionClub;
	private LicencieDAO gestionLicencie;
	// déclarations des éventuelles propriétés utiles au contrôleur
	public static List<Club> lesClubs = new ArrayList<Club>();
	public static List<Licencie> lesLicencies = new ArrayList<Licencie>();

	
	// Constructeur
	public ControleurPrincipal()
	{
		this.gestionClub=new ClubDAO();
		this.gestionLicencie= new LicencieDAO();
		// + initialisations éventuelles autres propriétés définies dans cette classe
		this.lesClubs=gestionClub.recupAll();
	}

	
	@Override
	public void actionPerformed(ActionEvent e) // Méthode qu'il faut implémenter
	{
		// quand on clique sur Quitter
		if (e.getActionCommand().equals("Quitter"))
				{
				Quitter();
				// déconnection bdd et arrêt de l’application
				}
		
		else if (e.getActionCommand().equals("comboBox")) {
			if (vue.comboBoxListe.getSelectedIndex()!= 0) {
				vue.affichageInfoClub(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1));
				//gestionLicencie.recupLicencie(lesLicencies.get(vue.comboBoxListe.getSelectedIndex()-1).getCode());
				 
				vue.afficher_licencie(gestionLicencie.recupLicencie(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1).getCode()));
				 
			}
			else {
				vue.lblAfficheInfoClub.setText("");
			}
		}
		// si on clique sur le bouton enregistrer (un nouveau licencié)
		else if (e.getActionCommand().equals("Ajouter"))
				{
			if (vue.comboBoxListe.getSelectedIndex()!= 0) {
					vue.ajouter();
			}
				// insertion d’un nouveau licencié dans la bdd en récupérant les données de la vue
				// retour à la vue avec mise à jour des licenciés du club dans la zone prévue
				}
		else if (e.getActionCommand().equals("Supprimer")) {
			vue.supprimer();
		}
		// si un autre évènement est à l’origine de l’appel au contrôleur
		else if (e.getActionCommand().equals("Supprimer un licencié"))
				{
		gestionLicencie.delete(gestionLicencie.read(vue.textCode.getText()));
		vue.PanelSupprimer.setVisible(false);
		vue.txtpnFondLicencie.setVisible(true);
		
		if (vue.comboBoxListe.getSelectedIndex()!= 0) {
			vue.affichageInfoClub(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1));
			//gestionLicencie.recupLicencie(lesLicencies.get(vue.comboBoxListe.getSelectedIndex()-1).getCode());
			 
			vue.afficher_licencie(gestionLicencie.recupLicencie(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1).getCode()));
			 
		}
		else {
			vue.lblAfficheInfoClub.setText("");
		}
				}
		
		else if (e.getActionCommand().equals("Enregistrer"))
		{
		gestionLicencie.create(new Licencie((vue.textNom.getText() + "." + vue.textPrenom.getText()), vue.textNom.getText(), vue.textPrenom.getText(), vue.textdateNaiss.getText(), lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1)));
		vue.PanelEnregistrer.setVisible(false);

		vue.txtpnFondLicencie.setVisible(true);
		vue.afficher_licencie(gestionLicencie.recupLicencie(lesClubs.get(vue.comboBoxListe.getSelectedIndex()-1).getCode()));
		}
	}
	
	public void Quitter() {
		ConnexionPostgreSql.Arreter(); 
		System.exit(0);
		
	}
	
	
	public void setVues(Vue_principale view) {
		this.vue=view;
		vue.remplirComboClub(lesClubs);
	
		
	}
}
