package dao;

import java.sql.*;
import java.util.*;

import classes.Licencie;

//d�finition de la classe d'acc�s aux donn�es de la table mvc.licencie
public class LicencieDAO extends DAO<Licencie> {

	@Override
	public List<Licencie> recupAll() {
		// d�finition de la liste qui sera retourn�e en fin de m�thode
		List<Licencie> listeClubs = new ArrayList<Licencie>(); 
		ClubDAO c = new ClubDAO();
		// d�claration de l'objet qui servira pour la requ�te SQL
		try {
			Statement requete = this.connect.createStatement();

			// d�finition de l'objet qui r�cup�re le r�sultat de l'ex�cution de la requ�te
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".club");

			// tant qu'il y a une ligne "r�sultat" � lire
			while (curseur.next()){
				// objet pour la r�cup d'une ligne de la table Club
				Licencie unLicencie = new Licencie();
				
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setDateNaiss(curseur.getString("date de naissance"));
				unLicencie.setLeClub(c.read(curseur.getString("leclub")));
				
				listeClubs.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeClubs; 
	}
	
	
	// Insertion d'un objet licencie dans la table licencie (1 ligne)
	@Override
	public void create(Licencie obj) {
		try {
			
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"mvc\".licencie VALUES(?, ?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1, obj.getCode());
				prepare.setString(2, obj.getNom());
				prepare.setString(3, obj.getPrenom());
				prepare.setString(4, obj.getDateNaiss());
				prepare.setString(5, obj.getLeClub().getCode());
					
				prepare.executeUpdate();  
					
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
		
		
	// Recherche d'un licencie par rapport � son code	
	@Override
	public Licencie read(String code) {
	
		Licencie leLicencie = new Licencie();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"mvc\".licencie WHERE code = '" + code +"'");
	          
	          if(result.first())
	        	  leLicencie = new Licencie(code, result.getString("nom"),result.getString("prenom"),result.getString("dateNaiss"), null);   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leLicencie;
		
	}
		
	
	// Mise � jour d'un licencie
	@Override
	public void update(Licencie obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"mvc\".licencie SET nom = '" + obj.getNom() + "'"+
	                    	      " WHERE code = '" + obj.getCode()+"'" );
				
			  obj = this.read(obj.getCode());
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
		
		
	}


	// Suppression d'un licencie
	@Override
	public void delete(Licencie obj) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"mvc\".licencie WHERE code = '" + obj.getCode()+"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}


	public List<Licencie> recupLicencie(String code) {
		
List<Licencie> listeClubs = new ArrayList<Licencie>(); 
		ClubDAO c = new ClubDAO();
		// d�claration de l'objet qui servira pour la requ�te SQL
		try {
			Statement requete = this.connect.createStatement();
			
			// d�finition de l'objet qui r�cup�re le r�sultat de l'ex�cution de la requ�te
			ResultSet curseur = requete.executeQuery("select * from mvc.licencie where leclub='"+code+"'");

			// tant qu'il y a une ligne "r�sultat" � lire
			while (curseur.next()){
				// objet pour la r�cup d'une ligne de la table Club
				Licencie unLicencie = new Licencie();
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setDateNaiss(curseur.getString("datenaiss"));
				unLicencie.setLeClub(c.read(curseur.getString("leclub")));
				
				listeClubs.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeClubs; 
	}
}


	


